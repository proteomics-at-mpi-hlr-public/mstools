#' A helper function to determine labeling efficiency from whole sets of
#' chemical labeling data that was searched by MaxQuant using the label(s) as
#' dynamic modifications
#'
#' @param paths A \code{\link{character}} object representing the paths of
#' \code{evidence.txt} file as produced by \code{MaxQuant}. If \code{NULL},
#' see \code{dir} parameter.
#' @param dir A single \code{\link{character}} object representing the path to a
#' directory in which \code{MaxQuant}-produced \code{evidence.txt} files reside.
#' Only considered if \code{is.null(path)}). Uses \code{\link{list.files}} with
#' below \code{pattern} parameter.
#' @param pattern A single \code{\link{character}} object. See
#' \code{\link{list.files}} for details.
#' @param column_label_regex A \code{\link{character}} \code{\link{vector}}
#' representing regular expressions used to extract the appropriate columns
#' and gnerate labels (\bold{note the braces in the defaults!}). Only one of
#' these can match in any given analysis.
#' @param by_experiment A single \code{\link{logical}} object indicating whether
#' efficiency analysis is performed by experiment or not (see experimental
#' design of the underlying \code{mAxQuant} analysis).
#' @param rtf_file \code{NULL} or a single \code{\link{character}} object
#' representing the path to an richt text format (RTF) file (extension is
#' enforced).
#' @return Returns a \code{gt_tbl} if \code{is.null(rtf_file)}. Else  a rich
#' text format file is saved and the \code{gt_tbl} returned silently.
#' @seealso \code{\link{labeling_efficiency}}, \code{\link{list.files}}
#' @author Johannes Graumann <johannes.graumann@mpi-bn.mpg.de>
#' @examples
#' # Unpack example data
#' tmp_dir <- tempdir()
#' evidence_set <- unique(
#'   dirname(
#'     unzip(
#'       zipfile = system.file(file.path("extdata", "evidence.txt_set.zip"),
#'       package = "mstools", mustWork = TRUE), exdir = tmp_dir)))
#'
#' # Run the analysis
#' tabulate_labeling_efficiency(dir = evidence_set)
#' ## Redo, but forgo splitting by experiment - average the experiments
#' tabulate_labeling_efficiency(dir = evidence_set, by_experiment = FALSE)
#' @export
tabulate_labeling_efficiency <- function(
  paths = NULL,
  dir = switch(assertive.reflection::is_windows() && interactive(),
               utils::choose.dir(), NULL),
  pattern = ".txt",
  column_label_regex = c("^DynMod (TMT)\\d+plex[^\\s]*(\\d{3,3})$",
                         "^DynMod (Dimeth)[^\\s]+(\\d+)$"),
  by_experiment = TRUE,
  rtf_file = NULL) {
  # Check prerequisites -----------------------------------------------------
  if (!is.null(rtf_file)) {
    assertive.types::assert_is_a_string(rtf_file)
    if (!is.null(dir)) {
      dir_name <- dir
    } else if (!is.null(paths)) {
      dir_name <- paths %>% sapply(dirname) %>% magrittr::extract2(1)
    }

    rtf_file <- fs::path(dir_name, fs::path_file(rtf_file)) %>%
      fs::path_ext_set("rtf")
  }

  # Process -----------------------------------------------------------------
  # Generate the data.frame
  output <- labeling_efficiency_set(paths = paths, dir = dir, pattern = pattern,
      column_label_regex = column_label_regex, by_experiment = by_experiment)
  # Group it:
  if (by_experiment) {
    output %<>% dplyr::mutate(Experiment = paste("Experiment", Experiment)) %>%
      gt::gt(groupname_col = "Experiment")
  } else {
    output %<>% gt::gt()
  }
  # Return
  if (is.null(rtf_file)) return(output)
  gt::gtsave(output, filename = rtf_file)
  message("NOTE: Output has been written to '", rtf_file, "'.")
  invisible(output)
}

## quiets concerns of R CMD check re: the tidy variables
if (getRversion() >= "2.15.1")  utils::globalVariables(c("Experiment"))
