#' A helper function to determine labeling efficiency from whole sets of
#' chemical labeling data that was searched by MaxQuant using the label(s) as
#' dynamic modifications
#'
#' @param paths A \code{\link{character}} object representing the paths of
#' \code{evidence.txt} file as produced by \code{MaxQuant}. If \code{NULL},
#' see \code{dir} parameter.
#' @param dir A single \code{\link{character}} object representing the path to a
#' directory in which \code{MaxQuant}-produced \code{evidence.txt} files reside.
#' Only considered if \code{is.null(path)}). Uses \code{\link{list.files}} with
#' below \code{pattern} parameter.
#' @param pattern A single \code{\link{character}} object. See
#' \code{\link{list.files}} for details.
#' @param column_label_regex A \code{\link{character}} \code{\link{vector}}
#' representing regular expressions used to extract the appropriate columns
#' and gnerate labels (\bold{note the braces in the defaults!}). Only one of
#' these can match in any given analysis.
#' @param by_experiment A single \code{\link{logical}} object indicating whether
#' efficiency analysis is performed by experiment or not (see experimental
#' design of the underlying \code{mAxQuant} analysis).
#' @param file_name A single \code{\link{character}} object (if not \code{NULL})
#' representing the path the results are being written to as a tab-delimited
#' text file.
#' @return A \code{\link{data.frame}}, \code{link{invisible}} if
#' \code{!is.null(file_name)}.
#' @seealso \code{\link{labeling_efficiency}}, \code{\link{list.files}}
#' @author Johannes Graumann <johannes.graumann@mpi-bn.mpg.de>
#' @examples
#' # Unpack example data
#' tmp_dir <- tempdir()
#' evidence_set <- unique(
#'   dirname(
#'     unzip(
#'       zipfile = system.file(file.path("extdata", "evidence.txt_set.zip"),
#'       package = "mstools", mustWork = TRUE), exdir = tmp_dir)))
#'
#' # Run the analysis
#' labeling_efficiency_set(dir = evidence_set)
#' ## Redo, but forgo splitting by experiment - average the experiments
#' labeling_efficiency_set(dir = evidence_set, by_experiment = FALSE)
#' @export
labeling_efficiency_set <- function(
  paths = NULL,
  dir,
  pattern = ".txt",
  column_label_regex = c("^DynMod (TMT)\\d+plex[^\\s]*(\\d{3,3})$",
                         "^DynMod (Dimeth)[^\\s]+(\\d+)$"),
  by_experiment = TRUE,
  file_name = NULL){
  # Check prerequisites -----------------------------------------------------
  if (!is.null(paths)) {
    assertive.types::assert_is_character(paths)
  }

  if (is.null(paths)) {
    assertive.types::assert_is_a_string(dir)
    assertive.files::assert_all_are_dirs(dir)

    assertive.types::assert_is_a_string(pattern)

    paths <- list.files(path = dir, pattern = pattern, full.names = TRUE)
  }
  assertive.numbers::assert_all_are_greater_than_or_equal_to(length(paths), 1)
  if (!assertive.reflection::is_windows())
    assertive.files::assert_all_are_readable_files(paths)

  assertive.types::assert_is_character(column_label_regex)

  assertive.types::assert_is_a_bool(by_experiment)

  if (!is.null(file_name)) assertive.types::assert_is_a_string(file_name)

  # Process -----------------------------------------------------------------
  # Read the files in
  evidences <- lapply(paths, function(x) {
    labeling_efficiency(x,
    column_label_regex = column_label_regex, by_experiment = by_experiment)
  })

  # if (by_experiment) {
  #   assertive.base::assert_engine(
  #     assertive.base::is_true,
  #     evidences %>% lapply(magrittr::extract2, "Experiment") %>% unique() %>%
  #       length() %>% magrittr::equals(1),
  #     msg = "Not all 'Experiment' entries in the set are equal.")
  # }

  # Combine the results
  evidences %<>% dplyr::bind_rows()

  # Order it
  evidences %<>% dplyr::arrange(Label)

  # Return (and/or write out)
  if (!is.null(file_name)) {
    utils::write.table(evidences, file = file_name, sep = "\t",
                       col.names = TRUE, row.names = FALSE)
    invisible(evidences)
  }
  return(evidences)
}

if (getRversion() >= "2.15.1")  utils::globalVariables(c("Label"))
